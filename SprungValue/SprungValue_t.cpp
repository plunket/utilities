/**
 * Unit tests for SprungValue.
 *
 * \author Tom Plunket <tom@mightysprite.com>
 * \copyright (c) 2019-2020 Tom Plunket, all rights reserved
 *
 * Licensed under the MIT/X license. Do with these files what you will but leave this header intact.
 */

#include "SprungValue.h"

#include "TestVector2.h"

#include <cmath>

#include "catch/catch.hpp"

TEST_CASE( "A single floating point value." )
{
    float testValue = GENERATE(0.0f, 1.0f, 2.0f, 0.5f, 1000.0f, 1e20, -1.0f, -47.25f);

    SECTION( "...with no (default) spring tension." )
    {
        SprungValue<float> v(testValue);
        CHECK(v.GetValue() == testValue);
        CHECK(v.GetVelocity() == 0);

        v.Tick(0);
        CHECK(v.GetValue() == testValue);
        CHECK(v.GetVelocity() == 0);

        v.Tick(1);
        CHECK(v.GetValue() == testValue);
        CHECK(v.GetVelocity() == 0);

        SECTION( "Doesn't move when goal changes." )
        {
            v.SetGoal(-80.0f);
            v.Tick(1);
            CHECK(v.GetValue() == testValue);
            CHECK(v.GetVelocity() == 0);
        }
    }

    SECTION( "with spring tension" )
    {
        SECTION( "Doesn't move when goal and starting value are the same." )
        {
            SprungValue<float> v(testValue, 10.0f);
            CHECK(v.GetValue() == testValue);
            CHECK(v.GetVelocity() == 0);

            v.Tick(0);
            CHECK(v.GetValue() == testValue);
            CHECK(v.GetVelocity() == 0);

            v.Tick(1);
            CHECK(v.GetValue() == testValue);
            CHECK(v.GetVelocity() == 0);
        }
    }

    SECTION( "permuting it all" )
    {
        if ((testValue != 0.0f) && (testValue < 1e10f))
        {
            float testValue2 = GENERATE(1.0f, 2.0f, 0.5f, 1000.0f);
            if (testValue < 0) testValue = -testValue;

            SprungValue<float> v(testValue, testValue2);
            v.SetGoal(0);

            CHECK(v.GetValue() == testValue);
            CHECK(v.GetVelocity() == 0);

            float lastVal = v.GetValue();
            v.Tick(0);
            CHECK(v.GetValue() == lastVal);
            CHECK(v.GetVelocity() == 0);

            lastVal = v.GetValue();
            v.Tick(1.0f);
            CHECK(v.GetValue() < lastVal);
            CHECK(v.GetValue() >= 0);
            CHECK(v.GetVelocity() <= 0);

            lastVal = v.GetValue();
            v.Tick(1.0f);
            CHECK(v.GetValue() <= lastVal);
            CHECK(v.GetValue() >= 0);
            CHECK(v.GetVelocity() <= 0);

            lastVal = v.GetValue();
            v.Tick(1.0f);
            CHECK(v.GetValue() <= lastVal);
            CHECK(v.GetValue() >= 0);
            CHECK(v.GetVelocity() <= 0);

            lastVal = v.GetValue();
            v.Tick(1.0f);
            CHECK(v.GetValue() <= lastVal);
            CHECK(v.GetValue() >= 0);
            CHECK(v.GetVelocity() <= 0);
        }
    }
}

TEST_CASE( "huge values" )
{
    const float k_largeValue = 90000000.0f;

    SECTION( "big moves" )
    {
        SprungValue<float> v(k_largeValue, 1.0f);
        v.SetGoal(0);

        CHECK(v.GetValue() == k_largeValue);
        CHECK(v.GetVelocity() == 0);

        float lastVal = v.GetValue();
        v.Tick(0);
        CHECK(v.GetValue() == lastVal);
        CHECK(v.GetVelocity() == 0);

        v.Tick(1.0f);
        CHECK(v.GetValue() < lastVal);
        CHECK(v.GetValue() > 0);
        CHECK(v.GetVelocity() < 0);

        lastVal = v.GetValue();
        v.Tick(1.0f);
        CHECK(v.GetValue() < lastVal);
        CHECK(v.GetValue() > 0);
        CHECK(v.GetVelocity() < 0);

        lastVal = v.GetValue();
        v.Tick(1.0f);
        CHECK(v.GetValue() < lastVal);
        CHECK(v.GetValue() > 0);
        CHECK(v.GetVelocity() < 0);

        lastVal = v.GetValue();
        v.Tick(1.0f);
        CHECK(v.GetValue() < lastVal);
        CHECK(v.GetValue() > 0);
        CHECK(v.GetVelocity() < 0);
    }

    SECTION( "heavy tension" )
    {
        SprungValue<float> v(10.0f, k_largeValue);
        v.SetGoal(0);

        CHECK(v.GetValue() == 10.0f);
        CHECK(v.GetVelocity() == 0);

        float lastVal = v.GetValue();
        v.Tick(0);
        CHECK(v.GetValue() == lastVal);
        CHECK(v.GetVelocity() == 0);

        v.Tick(1.0f);
        CHECK(v.GetValue() < lastVal);
        CHECK(v.GetValue() >= 0);
        CHECK(v.GetVelocity() <= 0);

        lastVal = v.GetValue();
        v.Tick(1.0f);
        CHECK(v.GetValue() <= lastVal);
        CHECK(v.GetValue() >= 0);
        CHECK(v.GetVelocity() <= 0);

        lastVal = v.GetValue();
        v.Tick(1.0f);
        CHECK(v.GetValue() <= lastVal);
        CHECK(v.GetValue() >= 0);
        CHECK(v.GetVelocity() <= 0);

        lastVal = v.GetValue();
        v.Tick(1.0f);
        CHECK(v.GetValue() <= lastVal);
        CHECK(v.GetValue() >= 0);
        CHECK(v.GetVelocity() <= 0);
    }

    SECTION( "Both!" )
    {
        SprungValue<float> v(k_largeValue, k_largeValue);
        v.SetGoal(0);

        CHECK(v.GetValue() == k_largeValue);
        CHECK(v.GetVelocity() == 0);

        float lastVal = v.GetValue();
        v.Tick(0);
        CHECK(v.GetValue() == lastVal);
        CHECK(v.GetVelocity() == 0);

        v.Tick(1.0f);
        CHECK(v.GetValue() <= lastVal);
        CHECK(v.GetValue() >= 0);
        CHECK(v.GetVelocity() <= 0);

        lastVal = v.GetValue();
        v.Tick(1.0f);
        CHECK(v.GetValue() <= lastVal);
        CHECK(v.GetValue() >= 0);
        CHECK(v.GetVelocity() <= 0);

        lastVal = v.GetValue();
        v.Tick(1.0f);
        CHECK(v.GetValue() <= lastVal);
        CHECK(v.GetValue() >= 0);
        CHECK(v.GetVelocity() <= 0);

        lastVal = v.GetValue();
        v.Tick(1.0f);
        CHECK(v.GetValue() <= lastVal);
        CHECK(v.GetValue() >= 0);
        CHECK(v.GetVelocity() <= 0);
    }
}

TEST_CASE( "A simple vector type." )
{
    auto testValue = GENERATE(TestVector2{0,0},
                              TestVector2{1.0,1.0},
                              TestVector2{99.0,-38.5});

    SECTION( "with no spring tension." )
    {
        SprungValue<TestVector2> v(testValue);
        v.SetGoal(TestVector2::zero);
        CHECK(v.GetValue() == testValue);
        CHECK(v.GetVelocity() == TestVector2::zero);

        v.Tick(0);
        CHECK(v.GetValue() == testValue);
        CHECK(v.GetVelocity() == TestVector2::zero);

        v.Tick(1);
        CHECK(v.GetValue() == testValue);
        CHECK(v.GetVelocity() == TestVector2::zero);
    }

    SECTION( "with some spring tension." )
    {
        if (testValue != TestVector2::zero)
        {
            SprungValue<TestVector2> v(testValue, 5.0f);
            v.SetGoal(TestVector2::zero);
            CHECK(v.GetValue() == testValue);
            CHECK(v.GetVelocity() == TestVector2::zero);

            float lastLen = lengthSquared(v.GetValue());
            v.Tick(0);
            CHECK(v.GetValue() == testValue);
            CHECK(v.GetVelocity() == TestVector2::zero);

            v.Tick(1);
            CHECK(lengthSquared(v.GetValue()) <= lastLen);
            // velocity and value are pointing in opposite directions.
            CHECK(dotProduct(v.GetValue(), v.GetVelocity()) < 0);

            lastLen = lengthSquared(v.GetValue());
            v.Tick(1);
            CHECK(lengthSquared(v.GetValue()) <= lastLen);
            CHECK(dotProduct(v.GetValue(), v.GetVelocity()) < 0);
        }
    }
}

