/**
 * Unit tests for the various interpolators.
 *
 * \author Tom Plunket <tom@mightysprite.com>
 * \copyright (c) 2020-2021 Tom Plunket, all rights reserved
 *
 * Licensed under the MIT/X license. Do with these files what you will but leave this header intact.
 */

#include "Interpolators.h"

#include "catch/catch.hpp"

#include "TestVector2.h"

TEST_CASE( "Linear interpolator" )
{
    TestVector2 v1 { 1.0f, 1.0f };
    TestVector2 v2 { 2.0f, 0.0f };

    CHECK(InterpolateLinear(0.0f, 1.0f, 0.0f) == 0.0f);
    CHECK(InterpolateLinear(0.0, 1.0, 0.0) == 0.0);
    CHECK(InterpolateLinear(TestVector2::zero, v1, 0.0) == TestVector2::zero);

    CHECK(InterpolateLinear(0.0f, 1.0f, 1.0f) == 1.0f);
    CHECK(InterpolateLinear(0.0, 1.0, 1.0) == 1.0);
    CHECK(InterpolateLinear(TestVector2::zero, v1, 1.0) == v1);
    CHECK(InterpolateLinear(TestVector2::zero, v2, 1.0) == v2);

    CHECK(InterpolateLinear(0.0f, 1.0f, 0.5f) == 0.5f);
    CHECK(InterpolateLinear(0.0, 1.0, 0.5) == 0.5);
    CHECK(InterpolateLinear(TestVector2::zero, v1, 0.5f) == TestVector2{0.5f, 0.5f});
    CHECK(InterpolateLinear(TestVector2::zero, v2, 0.5f) == TestVector2{1.0f, 0.0f});

    CHECK(InterpolateLinear(0.0f, 1.0f, 0.25f) == 0.25f);
    CHECK(InterpolateLinear(0.0, 1.0, 0.25) == 0.25);
    CHECK(InterpolateLinear(TestVector2::zero, v1, 0.25f) == TestVector2{0.25f, 0.25f});
    CHECK(InterpolateLinear(TestVector2::zero, v2, 0.25f) == TestVector2{0.5f, 0.0f});

    CHECK(InterpolateLinear(0.0f, 1.0f, 0.75f) == 0.75f);
    CHECK(InterpolateLinear(0.0, 1.0, 0.75) == 0.75);
    CHECK(InterpolateLinear(TestVector2::zero, v1, 0.75f) == TestVector2{0.75f, 0.75f});
    CHECK(InterpolateLinear(TestVector2::zero, v2, 0.75f) == TestVector2{1.5f, 0.0f});
}

TEST_CASE( "Cubic interpolator" )
{
    TestVector2 v1 { 1.0f, 1.0f };
    TestVector2 v2 { 2.0f, 0.0f };

    CHECK(InterpolateCubic(0.0f, 1.0f, 0.0f) == 0.0f);
    CHECK(InterpolateCubic(0.0, 1.0, 0.0) == 0.0);
    CHECK(InterpolateCubic(TestVector2::zero, v1, 0.0) == TestVector2::zero);

    CHECK(InterpolateCubic(0.0f, 1.0f, 1.0f) == 1.0f);
    CHECK(InterpolateCubic(0.0, 1.0, 1.0) == 1.0);
    CHECK(InterpolateCubic(TestVector2::zero, v1, 1.0) == v1);
    CHECK(InterpolateCubic(TestVector2::zero, v2, 1.0) == v2);

    CHECK(InterpolateCubic(0.0f, 1.0f, 0.5f) == 0.5f);
    CHECK(InterpolateCubic(0.0, 1.0, 0.5) == 0.5);
    CHECK(InterpolateCubic(TestVector2::zero, v1, 0.5f) == TestVector2{0.5f, 0.5f});
    CHECK(InterpolateCubic(TestVector2::zero, v2, 0.5f) == TestVector2{1.0f, 0.0f});

    CHECK(InterpolateCubic(0.0f, 1.0f, 0.25f) < 0.25f);
    CHECK(InterpolateCubic(0.0, 1.0, 0.25) < 0.25);
    CHECK(InterpolateCubic(TestVector2::zero, v1, 0.25f).x < 0.25f);
    CHECK(InterpolateCubic(TestVector2::zero, v2, 0.25f).x < 0.5f);

    CHECK(InterpolateCubic(0.0f, 1.0f, 0.75f) > 0.75f);
    CHECK(InterpolateCubic(0.0, 1.0, 0.75) > 0.75);
    CHECK(InterpolateCubic(TestVector2::zero, v1, 0.75f).y > 0.75f);
    CHECK(InterpolateCubic(TestVector2::zero, v2, 0.75f).x > 1.5f);
}

TEST_CASE( "Cosine interpolator" )
{
    TestVector2 v1 { 1.0f, 1.0f };
    TestVector2 v2 { 2.0f, 0.0f };

    CHECK(InterpolateCosine(0.0f, 1.0f, 0.0f) == 0.0f);
    CHECK(InterpolateCosine(0.0, 1.0, 0.0) == 0.0);
    CHECK(InterpolateCosine(TestVector2::zero, v1, 0.0) == TestVector2::zero);

    CHECK(InterpolateCosine(0.0f, 1.0f, 1.0f) == 1.0f);
    CHECK(InterpolateCosine(0.0, 1.0, 1.0) == 1.0);
    CHECK(InterpolateCosine(TestVector2::zero, v1, 1.0) == v1);
    CHECK(InterpolateCosine(TestVector2::zero, v2, 1.0) == v2);

    CHECK(InterpolateCosine(0.0f, 1.0f, 0.5f) == 0.5f);
    CHECK(InterpolateCosine(0.0, 1.0, 0.5) == 0.5);
    CHECK(InterpolateCosine(TestVector2::zero, v1, 0.5f) == TestVector2{0.5f, 0.5f});
    CHECK(InterpolateCosine(TestVector2::zero, v2, 0.5f) == TestVector2{1.0f, 0.0f});

    CHECK(InterpolateCosine(0.0f, 1.0f, 0.25f) < 0.25f);
    CHECK(InterpolateCosine(0.0, 1.0, 0.25) < 0.25);
    CHECK(InterpolateCosine(TestVector2::zero, v1, 0.25f).x < 0.25f);
    CHECK(InterpolateCosine(TestVector2::zero, v2, 0.25f).x < 0.5f);

    CHECK(InterpolateCosine(0.0f, 1.0f, 0.75f) > 0.75f);
    CHECK(InterpolateCosine(0.0, 1.0, 0.75) > 0.75);
    CHECK(InterpolateCosine(TestVector2::zero, v1, 0.75f).y > 0.75f);
    CHECK(InterpolateCosine(TestVector2::zero, v2, 0.75f).x > 1.5f);
}
