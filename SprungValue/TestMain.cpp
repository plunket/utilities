/**
 * Test runner mainline.
 *
 * \author Tom Plunket <tom@mightysprite.com>
 * \copyright (c) 2020-2021 Tom Plunket, all rights reserved
 *
 * Licensed under the MIT/X license. Do with these files what you will but leave this header intact.
 */

#include "SprungValue.h"

#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"
