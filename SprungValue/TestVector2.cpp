/**
 * An implementation of Vector2 for testing.
 *
 * \author Tom Plunket <tom@mightysprite.com>
 * \copyright (c) 2020-2021 Tom Plunket and Mighty Sprite Inc., all rights reserved.
 *
 * Licensed under the MIT/X license. Do with these files what you will but leave this header intact.
 */

#include "TestVector2.h"

#include <ostream>

const TestVector2 TestVector2::zero { 0, 0 };

std::ostream& operator<<(std::ostream& os, const TestVector2& v)
{
    os << '{' << v.x << ',' << v.y << '}';
    return os;
}

