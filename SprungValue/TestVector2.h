/**
 * An implementation of Vector2 for testing.
 *
 * \author Tom Plunket <tom@mightysprite.com>
 * \copyright (c) 2020 Tom Plunket and Mighty Sprite Inc., all rights reserved.
 *
 * Licensed under the MIT/X license. Do with these files what you will but leave this header intact.
 */

#pragma once

#include <ostream>

struct TestVector2
{
    float x, y;

    static const TestVector2 zero;
};

inline bool operator==(const TestVector2& lhs, const TestVector2& rhs)
{
    return (lhs.x == rhs.x) && (lhs.y == rhs.y);
}

inline bool operator!=(const TestVector2& lhs, const TestVector2& rhs)
{
    return (lhs.x != rhs.x) || (lhs.y != rhs.y);
}

inline TestVector2 operator+(const TestVector2& lhs, const TestVector2 & rhs)
{
    return { lhs.x + rhs.x, lhs.y + rhs.y };
}

inline TestVector2 operator-(const TestVector2& lhs, const TestVector2 & rhs)
{
    return { lhs.x - rhs.x, lhs.y - rhs.y };
}

inline TestVector2 operator*(const TestVector2& lhs, float scale)
{
    return { lhs.x * scale, lhs.y * scale };
}

inline float dotProduct(const TestVector2& rhs, const TestVector2 lhs)
{
    return (rhs.x * lhs.x) + (rhs.y * lhs.y);
}

inline float lengthSquared(const TestVector2& v)
{
    return dotProduct(v, v);
}

std::ostream& operator<<(std::ostream&, const TestVector2&);
