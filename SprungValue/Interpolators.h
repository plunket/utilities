/**
 * Various interpolators.
 *
 * \author Tom Plunket <tom@mightysprite.com>
 * \copyright (c) 2020-2021 Tom Plunket, all rights reserved.
 *
 * Licensed under the MIT/X license. Do with these files what you will but leave this header intact.
 */

#ifndef Interpolators_h
#define Interpolators_h
#pragma once
#define _USE_MATH_DEFINES

#include <cmath>

template <typename T>
T InterpolateLinear(T a, T b, float alpha)
{
    return (a * (1.0f - alpha)) + (b * alpha);
}

template <typename T>
T InterpolateCubic(T a, T b, float alpha)
{
    alpha = (3 * alpha * alpha) - (2 * alpha * alpha * alpha);
    return (a * (1.0f - alpha)) + (b * alpha);
}

template <typename T>
T InterpolateCosine(T a, T b, float alpha)
{
    alpha = (1.0f - std::cos(alpha * static_cast<float>(M_PI))) / 2.0f;
    return (a * (1.0f - alpha)) + (b * alpha);
}
#endif // ndef Interpolators_h
