/**
 * A drop-in LogTarget that can be used for basic message logging in
 * applications that use libc or the otherwise standard C runtime library.
 *
 * \author Tom Plunket <tom@mightysprite.com>
 * \copyright (c) 2017-2020 Tom Plunket, all rights reserved
 *
 * Licensed under the MIT/X license. Do with these files what you will but leave this header intact.
 */

#ifndef PrintfLogTarget_hpp
#define PrintfLogTarget_hpp

#include "LogTarget.hpp"
#include <cstdio>

struct PrintfLogTarget : public LogTarget
{
    explicit PrintfLogTarget(bool annotate=false) : annotate(annotate) {}

private:
    void LogMessage(char const* message, LogType lt, char const* file, unsigned int line)
    {
		static const char k_typeIndicators[k_numLogTypes] = { 'E', 'W', 'I', 'S' };
        if (annotate)
            printf("%s(%d): %c: %s", file, line, k_typeIndicators[lt], message);
        else
            printf("%s", message);
    }

    bool annotate;
};

#endif // ndef PrintfLogTarget_hpp
