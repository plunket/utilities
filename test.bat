@echo off
setlocal

set "COMPILER=Visual Studio"

::set "COMPILER_VERSION=11 2012"
::set "COMPILER_VERSION=14 2015"
::set "COMPILER_VERSION=15 2017"
set "COMPILER_VERSION=16 2019"

::set COMPILER_ARCHITECTURE=Win64
::set COMPILER_ARCHITECTURE=ARM
set COMPILER_ARCHITECTURE_NEW=x64

set CONFIG=Debug
set CONFIG=Release

if "%COMPILER_ARCHITECTURE%"=="" (
    set GENERATOR="%COMPILER% %COMPILER_VERSION%"
) else (
    set GENERATOR="%COMPILER% %COMPILER_VERSION% %COMPILER_ARCHITECTURE%"
)

if not "%COMPILER_ARCHITECTURE_NEW"=="" (
    set ARCHITECTURE=-A %COMPILER_ARCHITECTURE_NEW%
)

pushd %~dp0

set "PROJECT=%~1"
set PROJECT_DIR=_out\%~1\vs%COMPILER_VERSION:~0,2%.%COMPILER_ARCHITECTURE%%COMPILER_ARCHITECTURE_NEW%

if "%PROJECT%"=="" (
    echo Must specify a project name on the command line.
    exit /b 1
)

if NOT EXIST "%PROJECT%" (
    echo Project named %PROJECT% does not appear to exist.
    exit /b 2
)

if "%PROJECT%"=="Log" set EXTRA=-DUSE_wchar_t=off

echo on
cmake -G %GENERATOR% %ARCHITECTURE% %EXTRA% -S "%PROJECT%" -B "%PROJECT_DIR%" && cmake --build "%PROJECT_DIR%" --config %CONFIG% -j
@echo off
if NOT ERRORLEVEL 1 (
    if EXIST "%PROJECT_DIR%\%CONFIG%\%PROJECT%Tests.exe" (
        echo Running %PROJECT%Tests
        "%PROJECT_DIR%\%CONFIG%\%PROJECT%Tests.exe"
    )
)
