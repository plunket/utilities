cd `dirname $0`

PROJECT=${1%/}
OUTDIR="_out/$1/make"

cmake -S ${PROJECT}/ -B ${OUTDIR} && make -C $OUTDIR && ${OUTDIR}/${PROJECT}Tests
